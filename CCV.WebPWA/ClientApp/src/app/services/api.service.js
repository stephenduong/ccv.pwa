"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var rxjs_1 = require("rxjs");
var http_1 = require("@angular/common/http");
var ApiService = /** @class */ (function () {
    function ApiService(http) {
        this.http = http;
        this.apiUrl = "http://localhost:49930/api/product/";
    }
    ApiService.prototype.getProduct = function () {
        try {
            return this.http.get(this.apiUrl + "GetAllProducts");
        }
        catch (error) {
            rxjs_1.throwError(error);
        }
    };
    ApiService.prototype.getProductById = function (productId) {
        try {
            var url = this.apiUrl + ("GetProductById/" + productId);
            var httpOptions = {
                headers: new http_1.HttpHeaders({
                    'Content-Type': 'application/json;charset=utf-8',
                    'DataType': 'json',
                    'Method': 'get',
                })
            };
            return this.http.get(url, httpOptions);
        }
        catch (error) {
            rxjs_1.throwError(error);
        }
    };
    ApiService.prototype.updateProduct = function (objProduct) {
        try {
            var httpOptions = {
                headers: new http_1.HttpHeaders({
                    'Content-Type': 'application/json;charset=utf-8',
                    'DataType': 'json',
                    'Method': 'post'
                })
            };
            var url = this.apiUrl + "UpdateProduct";
            var body = {
                productId: objProduct.productId,
                productName: objProduct.productName,
                description: objProduct.description,
                quality: objProduct.quality,
                price: objProduct.price,
                updatedBy: objProduct.updatedBy
            };
            return this.http.post(url, body, httpOptions);
        }
        catch (error) {
            rxjs_1.throwError(error);
        }
    };
    ApiService.prototype.createProduct = function (objProduct) {
        try {
            var httpOptions = {
                headers: new http_1.HttpHeaders({
                    'Content-Type': 'application/json;charset=utf-8',
                    'DataType': 'json',
                    'Method': 'post'
                })
            };
            var url = this.apiUrl + "CreateProduct";
            var body = {
                productId: objProduct.productId,
                productName: objProduct.productName,
                description: objProduct.description,
                quality: objProduct.quality,
                price: objProduct.price,
                updatedBy: objProduct.updatedBy
            };
            return this.http.post(url, body, httpOptions);
        }
        catch (error) {
            rxjs_1.throwError(error);
        }
    };
    ApiService.prototype.deleteProduct = function (productId) {
        try {
            var httpOptions = {
                headers: new http_1.HttpHeaders({
                    'Content-Type': 'application/json;charset=utf-8',
                    'DataType': 'json',
                    'Method': 'delete'
                })
            };
            var url = this.apiUrl + ("DeleteProduct/" + productId);
            return this.http.delete(url, httpOptions);
        }
        catch (error) {
            rxjs_1.throwError(error);
        }
    };
    ApiService.prototype.gimmeJokes = function () {
        return this.http.get('https://api.chucknorris.io/jokes/random');
    };
    ApiService = __decorate([
        core_1.Injectable({
            providedIn: 'root'
        })
    ], ApiService);
    return ApiService;
}());
exports.ApiService = ApiService;
//# sourceMappingURL=api.service.js.map