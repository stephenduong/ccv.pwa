import { Injectable } from '@angular/core'; 
import { HttpClient, HttpHeaders } from '@angular/common/http';  
import { Observable, throwError } from 'rxjs';
import { User } from '../models/user.model'; 

@Injectable({
  providedIn: 'root'
})

export class SystemService {
  systemUrl: string = "http://localhost:49930/api/system/";

  constructor(private http: HttpClient) { }

  login(userName: string, password: string): Observable<boolean> {
    try {
      let httpOptions = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json;charset=utf-8',
          'DataType': 'json',
          'Method': 'get'
        })
      };
      var url = this.systemUrl + `UserLogin`;
      var body = {
        userName: userName,
        password: password, 
      };
      return this.http.post<boolean>(url, body, httpOptions);
    }
    catch (error) {
      throwError(error)
    }
  }

}


