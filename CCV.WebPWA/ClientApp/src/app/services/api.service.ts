import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http'; 
import { Product } from "../models/product.model"; 
 
@Injectable({
  providedIn: 'root'
})

export class ApiService {
  apiUrl: string = "http://localhost:49930/api/product/";

  constructor(private http: HttpClient) { }

  getProduct(): Observable<Product[]> {
    try {
      return this.http.get<Product[]>(this.apiUrl + "GetAllProducts");
    }
    catch (error) {
      throwError(error)
    }
  }

  getProductById(productId: string): Observable<Product[]> {
    try {
      var url = this.apiUrl + `GetProductById/${productId}`;
      let httpOptions = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json;charset=utf-8', 
          'DataType': 'json',
          'Method': 'get',
        })
      };
      return this.http.get<Product[]>(url, httpOptions);
    }
    catch (error) {
      throwError(error)
    }
  }

  updateProduct(objProduct: Product): Observable<boolean> {
    try {
      let httpOptions = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json;charset=utf-8',
          'DataType': 'json',
          'Method': 'post'
        })
      };
      var url = this.apiUrl + `UpdateProduct`;
      var body = {
        productId: objProduct.productId,
        productName: objProduct.productName,
        description: objProduct.description,
        quality: objProduct.quality,
        price: objProduct.price,
        updatedBy: objProduct.updatedBy
      };
      return this.http.post<boolean>(url, body, httpOptions);
    }
    catch (error) {
      throwError(error)
    }
  }

  createProduct(objProduct: Product): Observable<boolean> {
    try {
      let httpOptions = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json;charset=utf-8',
          'DataType': 'json',
          'Method': 'post'
        })
      };
      var url = this.apiUrl + `CreateProduct`;
      var body = {
        productId: objProduct.productId,
        productName: objProduct.productName,
        description: objProduct.description,
        quality: objProduct.quality,
        price: objProduct.price,
        updatedBy: objProduct.updatedBy
      };
      return this.http.post<boolean>(url, body, httpOptions);
    }
    catch (error) {
      throwError(error)
    }
  }

  deleteProduct(productId: string): Observable<boolean> {
    try {
      let httpOptions = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json;charset=utf-8',
          'DataType': 'json',
          'Method': 'delete'
        })
      };
      var url = this.apiUrl + `DeleteProduct/${productId}`;
      return this.http.delete<boolean>(url, httpOptions);
    }
    catch (error) {
      throwError(error)
    }
  }

  gimmeJokes() {
    return this.http.get('https://api.chucknorris.io/jokes/random')
  }
}


