import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProductRoutingModule } from './product-list-routing.module';
import { ProductComponent } from './product-list.component';
 
import { AngularMaterialModule } from '../../module/material/angular-material.module'; 
import { DialogModule } from '../../component/product-list/product-list.component';
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    ProductRoutingModule,
    AngularMaterialModule,
    FormsModule
  ],
  declarations: [ProductComponent, DialogModule],
  entryComponents: [DialogModule], 
})
export class ProductModule { }

