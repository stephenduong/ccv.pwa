import { Component, Inject } from '@angular/core';
import { Product } from "../../models/product.model";
import { ApiService } from '../../services/api.service';
import { CommonService } from '../../common/common.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { SwUpdate } from '@angular/service-worker';

@Component({
  selector: 'app-product-list-component',
  templateUrl: './product-list.component.html',
  providers: [ApiService, MatSnackBar, MatDialog, CommonService]
})
export class ProductComponent {
  public products: Product[];
  constructor(updates: SwUpdate,
    private api: ApiService,
    private snackBar: MatSnackBar,
    private dialog: MatDialog,
    private common: CommonService) {
    updates.available.subscribe(event => {
      updates.activateUpdate().then(() => document.location.reload());
    });
  }

  public productDeleted: Product[] = [];

  ngOnInit(): void {
    this.getProduct();
  }

  getProduct(): void {
    this.common.OnProcess();
    this.api.getProduct()
      .toPromise()
      .then((response: any) => {
        this.products = response;
      })
      .catch(error => { console.log(error) })
      .finally(() => { this.common.StopProcess(); });
  }

  addProduct(): void {
    const dialogRef = this.dialog.open(DialogModule, {
      data: {
        productName: "",
        price: 1000,
        quality: 1,
        description: "",
        updatedDate: new Date().toLocaleDateString("en-GB"),
        updatedBy: "New_User",
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result.productName == "" || result.description == "")
        return;
      this.common.OnProcess();
      this.api.createProduct(result)
        .toPromise()
        .then((response: boolean) => {
          if (response) {
            this.snackBar.open("Create successful!", 'Close', { duration: 2000 })
          } else {
            this.snackBar.open("Create fail!", 'Close', { duration: 2000 })
          }
        })
        .catch(error => { console.log(error) })
        .finally(() => {
          this.getProduct();
          this.common.StopProcess();
        });
    });
  }

  editProduct(product: Product): void {
    const dialogRef = this.dialog.open(DialogModule, {
      data: {
        productId: product.productId,
        productName: product.productName,
        price: product.price,
        quality: product.quality,
        description: product.description,
        updatedDate: new Date().toLocaleDateString("en-GB"),
        updatedBy: product.updatedBy,
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      this.common.OnProcess();
      this.api.updateProduct(result)
        .toPromise()
        .then((response: boolean) => {
          if (response) {
            this.snackBar.open("Edit successful!", 'Close', { duration: 2000 })
          } else {
            this.snackBar.open("Edit fail!", 'Close', { duration: 2000})
          }
        })
        .catch(error => { console.log(error) })
        .finally(() => {
          this.getProduct();
          this.common.StopProcess();
        });
    });
  }

  deleteProduct(productId: string): void {
    this.common.OnProcess();
    this.api.deleteProduct(productId)
      .toPromise()
      .then((response: boolean) => {
        if (response) {
          this.snackBar.open("Delete successful!", 'Close', { duration: 2000 })
        } else {
          this.snackBar.open("Delete fail!", 'Close', { duration: 2000 })
        }
      })
      .catch(error => { console.log(error) })
      .finally(() => {
        this.getProduct();
        this.common.StopProcess();
      });
  }
}

@Component({
  selector: 'app-dialog',
  templateUrl: '../dialog/dialog-product.html',
  styles: ['.mat-form-field {margin-left: 8px;}']
})
export class DialogModule {
  constructor(
    public dialogRef: MatDialogRef<DialogModule>,
    @Inject(MAT_DIALOG_DATA) public data: Product) { }

  onClose(): void {
    this.dialogRef.close();
  }
} 
