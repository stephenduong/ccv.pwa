import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TextNoteComponent } from './text-note.component';

const routes: Routes = [
  { path: '', component: TextNoteComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TextNoteRoutingModule { }
