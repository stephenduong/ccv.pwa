import { Component } from '@angular/core';
import { MatTableDataSource } from '@angular/material';
import { TextNoteDaily } from "../../models/text-note-daily.model";

@Component({
  selector: 'app-list-user',
  templateUrl: './text-note.component.html',
  styleUrls: ['./text-note.component.css']
})
export class TextNoteComponent {
  public textNote: TextNoteDaily;
  constructor() { }

  dataSource = new MatTableDataSource<TextNoteDaily>(ELEMENT_DATA);
  displayedColumns: string[] = ["userName", "startDate",
    "endDate", "titleName", "mainCourse", "sideMeal", "water",];
  ngOnInit() { 
  }
}

const ELEMENT_DATA: TextNoteDaily[] = [
  {
    textnoteId: 1, userName: "Nguyễn Thị A",
    startDate:"22/03/2020 12:00", endDate: "22/03/2020 12:20",
    titleName: "Bữa ăn", mainCourse: 5,
    sideMeal: 3, water: 500,
    supportStatus: 2, locaiton: 2,
    userStatus: "Ăn chậm", userSolution: "Cố gắng ngồi đợi bác gái ăn xong.",
    createdBy: "Staff 4", roomNumber: "2F",
    notedBy: "Staff 4"
  },
  {
    textnoteId: 2, userName: "Nguyễn Văn C",
    startDate: "22/03/2020 12:10", endDate: "22/03/2020 12:30",
    titleName: "Bữa ăn", mainCourse: 4,
    sideMeal: 4, water: 500,
    supportStatus: 2, locaiton: 2,
    userStatus: "Ăn nhanh", userSolution: "Cố gắng ngồi đợi bác.",
    createdBy: "Staff 4", roomNumber: "2F",
    notedBy: "Staff 4"
  }
];
