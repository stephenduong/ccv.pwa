import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TextNoteRoutingModule } from './text-note-routing.module';
import { TextNoteComponent } from './text-note.component'; 
import { AngularMaterialModule } from '../../module/material/angular-material.module';

@NgModule({
  imports: [
    CommonModule,
    TextNoteRoutingModule,
    AngularMaterialModule
  ],
  declarations: [TextNoteComponent]
})
export class TextNoteModule { }
