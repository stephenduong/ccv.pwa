import { Component } from '@angular/core';
import { SwUpdate } from '@angular/service-worker';
import { ApiService } from '../../services/api.service';
import { CommonService } from '../../common/common.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent {
  title = 'SamplePWA';
  joke: any;

  constructor(updates: SwUpdate, private data: ApiService, private common: CommonService) {
    updates.available.subscribe(event => { 
      updates.activateUpdate().then(() => document.location.reload());
    });
  }

  ngOnInit() {
    this.common.OnProcess();
    this.data.gimmeJokes().toPromise()
      .then((res): any => {
        this.joke = res; 
      })
      .catch(error => { console.log(error) })
      .finally(() => { this.common.StopProcess(); }); 
  }
}
