import { Component } from '@angular/core';
import { Product } from "../../models/product.model";
import { ApiService } from '../../services/api.service';
import { CommonService } from '../../common/common.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-product-detail-component',
  templateUrl: './product-detail.component.html',
  providers: [ApiService, CommonService]
})
export class ProductDetailComponent {
  public href: string = "";
  public product: Product;
  public productId: string;
  constructor(private api: ApiService, private router: ActivatedRoute, private common: CommonService) { }
    
  ngOnInit(): void {
    this.common.OnProcess();
    this.router.paramMap
      .subscribe(params => {
        this.productId = params.get('productId');
      });
    this.api.getProductById(this.productId)
      .toPromise()
      .then((response: any) => { 
        this.product = response;
      })
      .catch(error => { console.log(error) })
      .finally(() => { this.common.StopProcess(); }
      );
  }
}
