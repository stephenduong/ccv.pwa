import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormInfoRoutingModule } from './form-info-routing.module';
import { FormInfoComponent } from './form-info.component';

@NgModule({
  imports: [
    CommonModule,
    FormInfoRoutingModule
  ],
  declarations: [FormInfoComponent]
})
export class FormInfoModule { }
