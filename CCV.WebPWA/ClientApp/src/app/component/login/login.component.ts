import { Component, OnInit } from '@angular/core'; 
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { SystemService } from '../../services/system.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit { 
  userForm: any;
  error: any;
  constructor(
    private formBuilder: FormBuilder,
    private system: SystemService,
    private router: Router
  ) {
    this.userForm = this.formBuilder.group({
      userName: ['', Validators.required],
      password: ['', Validators.required]
    });}


  ngOnInit() {  
  }

  onSubmit() {
    const user = this.userForm.value;
    if (user.userName != "" && user.password != "") {
      this.error = "";
      this.system.login(user.userName, user.password)
        .subscribe((res) => {
          if (res)
            this.router.navigateByUrl('/home');
          else
            this.error = "Something went wrong! Username or password incorrest";
        }
      );
    }
  } 
}
