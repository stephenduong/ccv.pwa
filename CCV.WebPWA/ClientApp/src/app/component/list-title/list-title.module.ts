import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ListTitleRoutingModule } from './list-title-routing.module';
import { ListTitleComponent } from './list-title.component'; 

@NgModule({
  imports: [
    CommonModule,
    ListTitleRoutingModule 
  ],
  declarations: [ListTitleComponent]
})
export class ListTitleModule { }
