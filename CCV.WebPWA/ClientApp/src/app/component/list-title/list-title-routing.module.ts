import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListTitleComponent } from './list-title.component';

const routes: Routes = [
  { path: '', component: ListTitleComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ListTitleRoutingModule { }
