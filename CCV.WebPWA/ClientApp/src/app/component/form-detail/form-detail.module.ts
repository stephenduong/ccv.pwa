import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormDetailRoutingModule } from './form-detail-routing.module';
import { FormDetailComponent } from './form-detail.component'; 

@NgModule({
  imports: [
    CommonModule,
    FormDetailRoutingModule 
  ],
  declarations: [FormDetailComponent]
})
export class FormDetailModule { }
