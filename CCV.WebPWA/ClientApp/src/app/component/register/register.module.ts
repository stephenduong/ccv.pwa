import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RegisterRoutingModule } from './register-routing.module';
import { RegisterComponent } from './register.component'; 
import { FlexLayoutModule } from '@angular/flex-layout';
import { AngularMaterialModule } from '../../module/material/angular-material.module';
@NgModule({
  imports: [
    CommonModule,
    RegisterRoutingModule,
    FlexLayoutModule,
    AngularMaterialModule 
  ],
  declarations: [RegisterComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class RegisterModule { }
