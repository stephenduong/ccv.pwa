import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  roles: any = ['Admin', 'Author', 'Reader'];
  selected: any;
  constructor() { }

  ngOnInit() {
  }

}
