"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var ListUserComponent = /** @class */ (function () {
    function ListUserComponent() {
        this.listStaff = ['Ali',
            'Beatriz',
            'Charles',
            'Diya',
            'Eric',
            'Fatima',
            'Gabriel'];
    }
    ;
    ListUserComponent.prototype.handleSelection = function (event) {
        console.log(event);
        if (event.option.selected) {
            event.source.deselectAll();
            event.option._setSelected(true);
        }
    };
    ListUserComponent = __decorate([
        core_1.Component({
            selector: 'app-list-user',
            templateUrl: './list-user.component.html',
            styleUrls: ['./list-user.component.css']
        })
    ], ListUserComponent);
    return ListUserComponent;
}());
exports.ListUserComponent = ListUserComponent;
//# sourceMappingURL=list-user.component.js.map