import { Component } from '@angular/core'; 

@Component({
  selector: 'app-list-user',
  templateUrl: './list-user.component.html',
  styleUrls: ['./list-user.component.css']
})
export class ListUserComponent { 
  constructor() {};

  listStaff: string[] = ['Ali',
    'Beatriz',
    'Charles',
    'Diya',
    'Eric',
    'Fatima',
    'Gabriel'];

  handleSelection(event: any) {
    console.log(event);
    if (event.option.selected) {
      event.source.deselectAll();
      event.option._setSelected(true);
    }
  }
}
