export interface Product {
  productId: string;
  price: number;
  quality: number;
  description: string;
  productName: string;
  updatedBy: string;
  updatedDate: Date;
} 
