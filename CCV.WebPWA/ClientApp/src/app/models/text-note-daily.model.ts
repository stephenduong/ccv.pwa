export interface TextNoteDaily {
  textnoteId: number; 
  userName: string;
  //bắt đầu
  startDate: string;
  //kết thúc
  endDate: string;
  //tên biểu mẩu
  titleName: string;
  //bữa chính
  mainCourse: number;
  //bữa phụ
  sideMeal: number; 
  //lượng nước
  water: number;
  //hộ lý hỗ trợ
  supportStatus: number;
  //địa điểm
  locaiton: number;
  //tình trạng
  userStatus: string;
  //giải pháp
  userSolution: string;
  //người tạo
  createdBy: string;
  //phòng
  roomNumber: string;
  //người ghi chép
  notedBy: string;
}  
