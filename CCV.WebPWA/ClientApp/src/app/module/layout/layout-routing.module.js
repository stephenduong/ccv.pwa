"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var main_layout_component_1 = require("./main-layout/main-layout.component");
var footer_only_layout_component_1 = require("./footer-only-layout/footer-only-layout.component");
var text_note_layout_component_1 = require("./text-note-layout/text-note-layout.component");
var routes = [{
        path: '',
        redirectTo: '/text-note',
        pathMatch: 'full'
    },
    {
        path: '',
        component: main_layout_component_1.MainLayoutComponent,
        children: [
            { path: 'home', loadChildren: '../../component/home/home.module#HomeModule' },
            { path: 'counter', loadChildren: '../../component/counter/counter.module#CounterModule' },
            { path: 'product-list', loadChildren: '../../component/product-list/product-list.module#ProductModule' },
            { path: 'product-detail/:productId', loadChildren: '../../component/product-detail/product-detail.module#ProductDetailModule' },
        ]
    },
    {
        path: '',
        component: footer_only_layout_component_1.FooterOnlyLayoutComponent,
        children: [
            { path: 'login', loadChildren: '../../component/login/login.module#LoginModule' },
            { path: 'register', loadChildren: '../../component/register/register.module#RegisterModule' }
        ]
    },
    {
        path: '',
        component: text_note_layout_component_1.TextNoteLayoutComponent,
        children: [
            { path: 'text-note', loadChildren: '../../component/text-note/text-note.module#TextNoteModule' },
        ]
    },
];
var LayoutRoutingModule = /** @class */ (function () {
    function LayoutRoutingModule() {
    }
    LayoutRoutingModule = __decorate([
        core_1.NgModule({
            imports: [router_1.RouterModule.forRoot(routes)],
            exports: [router_1.RouterModule]
        })
    ], LayoutRoutingModule);
    return LayoutRoutingModule;
}());
exports.LayoutRoutingModule = LayoutRoutingModule;
//# sourceMappingURL=layout-routing.module.js.map