import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-text-note-layout',
  templateUrl: './text-note-layout.component.html',
  styleUrls: ['./text-note-layout.component.css']
})
export class TextNoteLayoutComponent implements OnInit {

  constructor() {
    console.log('Text Note layout constructor called');
  }

  ngOnInit() {
  }

}
