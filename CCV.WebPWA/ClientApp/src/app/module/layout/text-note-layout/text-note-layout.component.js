"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var TextNoteLayoutComponent = /** @class */ (function () {
    function TextNoteLayoutComponent() {
        console.log('Text Note layout constructor called');
    }
    TextNoteLayoutComponent.prototype.ngOnInit = function () {
    };
    TextNoteLayoutComponent = __decorate([
        core_1.Component({
            selector: 'app-text-note-layout',
            templateUrl: './text-note-layout.component.html',
            styleUrls: ['./text-note-layout.component.css']
        })
    ], TextNoteLayoutComponent);
    return TextNoteLayoutComponent;
}());
exports.TextNoteLayoutComponent = TextNoteLayoutComponent;
//# sourceMappingURL=text-note-layout.component.js.map