import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainLayoutComponent } from './main-layout/main-layout.component';
import { FooterOnlyLayoutComponent } from './footer-only-layout/footer-only-layout.component';
import { FooterComponent } from './footer/footer.component';
import { NavMenuComponent } from '../../component/nav-menu/nav-menu.component'; 
import { ListUserComponent } from '../../component/list-user/list-user.component'; 
import { ListTitleComponent } from '../../component/list-title/list-title.component'; 
import { FormInfoComponent } from '../../component/form-info/form-info.component'; 
import { FormDetailComponent } from '../../component/form-detail/form-detail.component'; 
import { LayoutRoutingModule } from './layout-routing.module';

import { TextNoteLayoutComponent } from './text-note-layout/text-note-layout.component';

import { BrowserAnimationsModule} from '@angular/platform-browser/animations';

import { AngularMaterialModule } from '../../module/material/angular-material.module';

@NgModule({
  imports: [
    CommonModule,
    LayoutRoutingModule,
    BrowserAnimationsModule,
    AngularMaterialModule
  ],
  exports: [],
  declarations: [
    MainLayoutComponent,
    FooterOnlyLayoutComponent,
    NavMenuComponent,
    ListUserComponent,
    ListTitleComponent,
    FormInfoComponent,
    FormDetailComponent,
    FooterComponent,
    TextNoteLayoutComponent
  ]
})
export class LayoutModule { }
