import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainLayoutComponent } from './main-layout/main-layout.component';
import { FooterOnlyLayoutComponent } from './footer-only-layout/footer-only-layout.component';
import { TextNoteLayoutComponent } from './text-note-layout/text-note-layout.component';

const routes: Routes = [{
  path: '',
  redirectTo: '/text-note',
  pathMatch: 'full'
},
{
  path: '',
  component: MainLayoutComponent,
  children: [
    { path: 'home', loadChildren: '../../component/home/home.module#HomeModule' },
    { path: 'counter', loadChildren: '../../component/counter/counter.module#CounterModule' },
    { path: 'product-list', loadChildren: '../../component/product-list/product-list.module#ProductModule' },
    { path: 'product-detail/:productId', loadChildren: '../../component/product-detail/product-detail.module#ProductDetailModule' },  
  ]
},
{
  path: '',
  component: FooterOnlyLayoutComponent,
  children: [
    { path: 'login', loadChildren: '../../component/login/login.module#LoginModule' },
    { path: 'register', loadChildren: '../../component/register/register.module#RegisterModule' }
  ]
  },
  {
    path: '',
    component: TextNoteLayoutComponent,
    children: [
      { path: 'text-note', loadChildren: '../../component/text-note/text-note.module#TextNoteModule' }, 
    ]
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)], 
  exports: [RouterModule]
})

export class LayoutRoutingModule { }
