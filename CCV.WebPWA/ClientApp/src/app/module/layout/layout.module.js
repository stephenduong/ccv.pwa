"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var common_1 = require("@angular/common");
var main_layout_component_1 = require("./main-layout/main-layout.component");
var footer_only_layout_component_1 = require("./footer-only-layout/footer-only-layout.component");
var footer_component_1 = require("./footer/footer.component");
var nav_menu_component_1 = require("../../component/nav-menu/nav-menu.component");
var list_user_component_1 = require("../../component/list-user/list-user.component");
var list_title_component_1 = require("../../component/list-title/list-title.component");
var form_info_component_1 = require("../../component/form-info/form-info.component");
var form_detail_component_1 = require("../../component/form-detail/form-detail.component");
var layout_routing_module_1 = require("./layout-routing.module");
var text_note_layout_component_1 = require("./text-note-layout/text-note-layout.component");
var animations_1 = require("@angular/platform-browser/animations");
var angular_material_module_1 = require("../../module/material/angular-material.module");
var LayoutModule = /** @class */ (function () {
    function LayoutModule() {
    }
    LayoutModule = __decorate([
        core_1.NgModule({
            imports: [
                common_1.CommonModule,
                layout_routing_module_1.LayoutRoutingModule,
                animations_1.BrowserAnimationsModule,
                angular_material_module_1.AngularMaterialModule
            ],
            exports: [],
            declarations: [
                main_layout_component_1.MainLayoutComponent,
                footer_only_layout_component_1.FooterOnlyLayoutComponent,
                nav_menu_component_1.NavMenuComponent,
                list_user_component_1.ListUserComponent,
                list_title_component_1.ListTitleComponent,
                form_info_component_1.FormInfoComponent,
                form_detail_component_1.FormDetailComponent,
                footer_component_1.FooterComponent,
                text_note_layout_component_1.TextNoteLayoutComponent
            ]
        })
    ], LayoutModule);
    return LayoutModule;
}());
exports.LayoutModule = LayoutModule;
//# sourceMappingURL=layout.module.js.map