import { Injectable } from '@angular/core';


@Injectable({
  providedIn: 'root'
})

export class CommonService {
  //Loader process
  element: any = document.getElementById('loader-wrapper'); 

  OnProcess() {
    this.element.className = '';
  };
  StopProcess() {
    this.element.className = 'd-none';
  }
}
