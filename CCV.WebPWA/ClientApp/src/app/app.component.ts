import { Component } from '@angular/core';
//import { SwPush } from '@angular/service-worker/'; 
//import { NewsletterService } from "./services/newsletter.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'CCV';
  //sub: PushSubscription;

  //readonly VAPID_PUBLIC_KEY = "BMHxpkVQsXs6JTmohH1As5IDNbgo3shogFhr4f3b_zw2tX6yB2y9LKiEXso2q9CYa9bG-QwDBfH86q0qWmOlxWI";

  //constructor(
  //  private swPush: SwPush,
  //  private newsletterService: NewsletterService) { }

  //subscribeToNotifications() {

  //  this.swPush.requestSubscription({
  //    serverPublicKey: this.VAPID_PUBLIC_KEY
  //  })
  //    .then(sub => { 
  //      this.sub = sub;
         
  //      console.log("Notification Subscription: ", sub);

  //      this.newsletterService.addPushSubscriber(sub).subscribe(
  //        () => console.log('Sent push subscription object to server.'),
  //        err => console.log('Could not send subscription object to server, reason: ', err)
  //      ); 
  //    })
  //    .catch(err => console.error("Could not subscribe to notifications", err));
  //} 

  //sendNewsletter() {
  //  console.log("Sending Newsletter to all Subscribers ...");
  //  this.newsletterService.send().subscribe();
  //}
}
