import { BrowserModule } from '@angular/platform-browser';
import { NgModule} from '@angular/core';
import { FormsModule, ReactiveFormsModule  } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

// Routing layout 
import { LayoutModule } from './module/layout/layout.module';

//Grenaral
import { AppComponent } from './app.component';
import { environment } from '../environments/environment';
import { RouterModule } from '@angular/router';

//Service 
import { CommonService } from './common/common.service';

//Module
import { ServiceWorkerModule } from '@angular/service-worker';   

@NgModule({
  declarations: [
    AppComponent,  
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    HttpClientModule, FormsModule, ReactiveFormsModule,
    RouterModule.forRoot([]), 
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production }), 
    LayoutModule
  ],
  providers: [CommonService],
  bootstrap: [AppComponent],
})
export class AppModule { }
