﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using ProductAPI.Service.Interfaces;
using ProductAPI.Service.Model;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace CCV.APIService.Controllers
{
    [ApiController]
    [Route("api/system")]
    public class UserController : ControllerBase
    {
        #region -- Variables --

        private readonly ILogger<UserController> logger;

        private readonly IUserRepository userRepository;

        #endregion

        #region -- Constructor

        public UserController(
            ILogger<UserController> _logger,
            IUserRepository _userRepository)
        {
            logger = _logger;
            userRepository = _userRepository;
        }

        #endregion

        #region -- Action -- 

        [HttpPost]
        [Route("UserLogin")]
        public bool UserLogin([FromBody]User user)
        {
            try
            {  
                bool accept = userRepository.UserLogin(user.UserName, user.Password);
                if(accept)  
                    return true;
                return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #region -- Get --

        [HttpGet]
        [Route("GetAllUsers")]
        public async Task<List<User>> GetAllUsers()
        {
            try
            {
                return await userRepository.GetAllUsers();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpGet]
        [Route("GetUserById/{userId}")]
        public async Task<User> GetUserById(string userId)
        {
            try
            { 
                var user = await userRepository.GetUserByUserId(userId);
                if (user != null)
                    return user;
                return null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region -- Create --

        [HttpPost]
        [Route("CreateUser")]
        public bool CreateUser(User user)
        {
            try
            {
                userRepository.CreateUser(user);
                return true;
            }
            catch
            {
                return false;
            }
        }

        #endregion

        #region -- Update --

        [HttpPost]
        [Route("UpdateUser")]
        public async Task<bool> UpdateUser([FromBody]User user)
        {
            try
            {
                return await userRepository.UpdateUser(user);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region -- Delete --

        [HttpDelete]
        [Route("DeleteUser/{userId}")]
        public async Task<bool> DeleteUser(string userId)
        {
            try
            {
                return await userRepository.DeleteUser(userId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #endregion
    }
}
