﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using ProductAPI.Service.Interfaces;
using ProductAPI.Service.Model;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CCV.APIService.Controllers
{
    [ApiController]
    [Route("api/product")]
    public class ProductController : ControllerBase
    {
        #region -- Variables --

        private readonly ILogger<ProductController> logger;

        private readonly IProductRepository productRepository;

        #endregion

        #region -- Constructor

        public ProductController(
            ILogger<ProductController> _logger,
            IProductRepository _productRepository)
        {
            logger = _logger;
            productRepository = _productRepository;
        }

        #endregion

        #region -- Action --

        [Route("values")]
        public ActionResult Index()
        {
            return Content("API Service");
        }

        #region -- Get --

        [HttpGet]
        [Route("GetAllProducts")]
        public async Task<List<Product>> GetAllProducts()
        {
            try
            {
                return await productRepository.GetAllProducts();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpGet]
        [Route("GetProductById/{productId}")]
        public async Task<Product> GetProductById(string productId)
        {
            try
            {
                var productRepository = new ProductRepository();
                var product = await productRepository.GetProductByProductId(productId);
                if (product != null)
                    return product;
                return null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region -- Create --

        [HttpPost]
        [Route("CreateProduct")]
        public bool CreateProduct(Product product)
        {
            try
            {
                productRepository.CreateProduct(product);
                return true;
            }
            catch
            {
                return false;
            }
        }

        #endregion

        #region -- Update --

        [HttpPost]
        [Route("UpdateProduct")]
        public async Task<bool> UpdateProduct([FromBody]Product product)
        {
            try
            {
                return await productRepository.UpdateProduct(product);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region -- Delete --

        [HttpDelete]
        [Route("DeleteProduct/{productId}")]
        public async Task<bool> DeleteProduct(string productId)
        {
            try
            {
                return await productRepository.DeleteProduct(productId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #endregion
    }
}
