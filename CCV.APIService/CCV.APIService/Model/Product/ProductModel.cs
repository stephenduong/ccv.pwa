using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace ProductAPI.Service.Model
{
    public class Product
    {
        [BsonId]
        public ObjectId Id { get; set; }

        [Index]
        public string ProductId { get; set; }
        public string ProductName { get; set; }
        public int Quality { get; set; }
        public string Description { get; set; }
        public int Price { get; set; }
        public DateTime UpdatedDate { get; set; }
        public string UpdatedBy { get; set; }
    }
}
