using Microsoft.Extensions.Options;
using MongoDB.Driver;
using ProductAPI.Service.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace ProductAPI.Service.Model
{
    public class ProductRepository : IProductRepository
    { 
        private IMongoClient client;
        private IMongoDatabase db;
        private IMongoCollection<Product> productCollection;
        private readonly string connectionString = "mongodb+srv://admin:123@tinhdb-vld9t.mongodb.net/test?retryWrites=true&w=majority"; 

        public ProductRepository()
        { 
            client = new MongoClient(connectionString);
            db = client.GetDatabase("CCV");
            productCollection = db.GetCollection<Product>("Product");
        }

        public async Task<List<Product>> GetAllProducts()
        {
            try
            {
                var query = new QueryDocument();
                var result = await Task.FromResult(productCollection.Find(query).SortBy(f=>f.UpdatedDate).ToList());
                return result;
            }
            catch (Exception ex)
            { 
                throw ex;
            }
        }
        public async Task<Product> GetProductByProductId(string productId)
        {
            try
            {
                var query = new QueryDocument();
                var filter = Builders<Product>.Filter.Eq("ProductId",productId);
                var result = await productCollection.Find(filter).ToListAsync();
                if (result.Count <= 0)
                    return null;
                return result.FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async void CreateProduct(Product product)
        {
            try
            {
                product.ProductId = Guid.NewGuid().ToString();
                product.UpdatedDate = DateTime.Now;
                await productCollection.InsertOneAsync(product);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<bool> UpdateProduct(Product product)
        {
            try
            { 
                var filter = Builders<Product>.Filter.Eq("ProductId", product.ProductId);
                var update = Builders<Product>.Update
                    .Set(nameof(Product.ProductName),product.ProductName)
                    .Set(nameof(Product.Price),product.Price)
                    .Set(nameof(Product.Quality), product.Quality)
                    .Set(nameof(Product.Description), product.Description) 
                    .Set(nameof(Product.UpdatedDate), DateTime.Now)
                    .Set(nameof(Product.UpdatedBy), product.UpdatedBy);
                var updated = await productCollection.UpdateOneAsync(filter, update);
                return updated.ModifiedCount != 0;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<bool> DeleteProduct(string productId)
        {
            try
            { 
                var filter = Builders<Product>.Filter.Eq("ProductId", productId); 
                var updated = await productCollection.DeleteOneAsync(filter);
                return updated.DeletedCount != 0;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        } 
    }
}
