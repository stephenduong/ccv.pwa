using ProductAPI.Service.Model;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProductAPI.Service.Interfaces
{
    public interface IProductRepository 
    {
        Task<List<Product>> GetAllProducts();
        Task<Product> GetProductByProductId(string productId);
        void CreateProduct(Product product);
        Task<bool> UpdateProduct(Product product);
        Task<bool> DeleteProduct(string productId);
    }
}
