using Microsoft.AspNetCore.Mvc;
using MongoDB.Driver;
using ProductAPI.Service.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace ProductAPI.Service.Model
{
    public class UserRepository : IUserRepository
    { 
        private IMongoClient client;
        private IMongoDatabase db;
        private IMongoCollection<User> userCollection;
        private readonly string connectionString = "mongodb+srv://admin:123@tinhdb-vld9t.mongodb.net/test?retryWrites=true&w=majority"; 

        public UserRepository()
        { 
            client = new MongoClient(connectionString);
            db = client.GetDatabase("CCV");
            userCollection = db.GetCollection<User>("User");
        }

        public bool UserLogin(string userName, string password)
        {
            try
            {
                var query = new QueryDocument();
                var filter = Builders<User>.Filter.Eq("UserName", userName);
                var result = userCollection.Find(filter).FirstOrDefault();
                if (result != null)
                    return true;
                return false;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }


        public async Task<List<User>> GetAllUsers()
        {
            try
            {
                var query = new QueryDocument();
                var result = await Task.FromResult(userCollection.Find(query).SortBy(f=>f.CreatedDate).ToList());
                return result;
            }
            catch (Exception ex)
            { 
                throw ex;
            }
        }
        public async Task<User> GetUserByUserId(string userId)
        {
            try
            {
                var query = new QueryDocument();
                var filter = Builders<User>.Filter.Eq("UserId", userId);
                var result = await userCollection.Find(filter).ToListAsync();
                if (result.Count <= 0)
                    return null;
                return result.FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async void CreateUser(User user)
        {
            try
            {
                user.UserId = Guid.NewGuid().ToString(); 
                await userCollection.InsertOneAsync(user);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<bool> UpdateUser(User user)
        {
            try
            { 
                var filter = Builders<User>.Filter.Eq("UserId", user.UserId);
                var update = Builders<User>.Update
                    .Set(nameof(User.UserName), user.UserName)
                    .Set(nameof(User.Password), user.Password) 
                    .Set(nameof(User.CreatedDate), DateTime.Now);
                var updated = await userCollection.UpdateOneAsync(filter, update);
                return updated.ModifiedCount != 0;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<bool> DeleteUser(string userId)
        {
            try
            { 
                var filter = Builders<User>.Filter.Eq("UserId", userId); 
                var updated = await userCollection.DeleteOneAsync(filter);
                return updated.DeletedCount != 0;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        } 
    }
}
