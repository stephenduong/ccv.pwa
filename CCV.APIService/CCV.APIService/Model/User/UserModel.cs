using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace ProductAPI.Service.Model
{
    public class User
    {
        [BsonId]
        public ObjectId Id { get; set; }

        [Index]
        public string UserId { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; } 
        public DateTime CreatedDate { get; set; }
        public int Role { get; set; }

    }
}
