using Microsoft.AspNetCore.Mvc;
using ProductAPI.Service.Model;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProductAPI.Service.Interfaces
{
    public interface IUserRepository
    {
        bool UserLogin(string userName, string password);
        Task<List<User>> GetAllUsers();
        Task<User> GetUserByUserId(string userId);
        void CreateUser(User user);
        Task<bool> UpdateUser(User user);
        Task<bool> DeleteUser(string userId); 
    }
}
